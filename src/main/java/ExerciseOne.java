import java.util.ArrayList;

public class ExerciseOne {

    public static int[] findTwoSum(int[] list, int sum) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        ArrayList<Integer> listed = new ArrayList<Integer>();

        for (int i = 0; i < list.length; i++) {
            listed.add(list[i]);
        }

        for(int i = 0; i < listed.size(); i++){
            Integer reference = listed.get(i);

            for(int j = 0; j < listed.size(); j++){
                if(reference + listed.get(j) == sum){
                    result.add(listed.indexOf(reference));
                    result.add(listed.indexOf(listed.get(j)));
                    break;
                }
            }

            if(!result.isEmpty()){
                break;
            }
        }

        if(result.isEmpty()){
            return null;
        }

        int[] resultArray = new int[result.size()];

        for(int i = 0; i < resultArray.length; i++){
            resultArray[i] = result.get(i);
        }

        return resultArray;
    }

    public static void main(String[] args) {
        int[] indices = findTwoSum(new int[] { 1, 3, 5, 7, 9 }, 12);
        System.out.println(indices[0] + " " + indices[1]);
    }
}
