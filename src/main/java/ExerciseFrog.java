import java.util.ArrayList;
import java.util.Collections;

public class ExerciseFrog {

    public static int numberOfWays(int n) {
        //Result is minimun 1
        int result = 1;
        int sum = n;
        boolean end = false;

        ArrayList<Integer> referenceArray = new ArrayList<Integer>();

        for(int i = 0; i<n ; i++){
            referenceArray.add(1);
        }

        while(!end && sum == n){
            if(referenceArray.indexOf(1) != -1){
                referenceArray.remove(referenceArray.get(referenceArray.lastIndexOf(1)));
            }
            if(referenceArray.indexOf(1) != -1){
                referenceArray.remove(referenceArray.get(referenceArray.lastIndexOf(1)));
            }
            referenceArray.add(2);

            if(referenceArray.stream().mapToInt(value -> value).sum() == n){
                result ++;
                result += Collections.frequency(referenceArray, 2) * Collections.frequency(referenceArray, 1);

                sum = referenceArray.stream().mapToInt(value -> value).sum();
            }else{
                end = true;
            }
        }

        return result;
    }

    public static void main(String[] args) {
        System.out.println(numberOfWays(5));
    }
}
